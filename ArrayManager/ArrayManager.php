<?php
/**
 * ArrayManager provee funcionalidad para manipular arrays de una manera mas sencilla.
 * User: yesid
 * Date: 17/03/2016
 * Time: 3:44 PM
 */

namespace Rootcore\Helper\ArrayManager;

class ArrayManager
{
  /**
   * Busca un elemento determinado dentro del array.
   *
   * @param array $array El array donde se va a realizar la busqueda
   * @param mixed $item El elemento a buscar
   * @return mixed
   * @version 1.0
   * @since 2016-03-17 12:40:00
   * @author Yesid Durán G. (Yeligoth) <yesiddg2014@gmail.com>
   */
  public static function search($array, $item) {
    return array_search($item, $array);
  }

  /**
   * Determina si una llave existe o no en un array.
   *
   * @param array $array El array donde se va a realizar la busqueda
   * @param mixed $key La llave a buscar
   * @return boolean
   * @version 1.0
   * @since 2016-03-17 15:53:00
   * @author Yesid Durán G. (Yeligoth) <yesiddg2014@gmail.com>
   */
  public static function existsKey($array, $key) {
    return array_key_exists($key, $array);
  }

  /**
   * Divide un array en fragmentos del tamaño definido por el pogramador, la ultima parte
   * podra contener menos elementos que el tamaño especificado.
   *
   * @param array $array El array que se va a fragmentar.
   * @param int $size El numero de elementos en el que se va a dividir el array.
   * @param boolean $preserveKeys Indica si los indices se preservaran, por defecto se establece en false
   * lo que indica que, cada elemento se reordenara dentro de su fragmento, si se pasa en true, mantendra los indices reales.
   * @return boolean
   * @version 1.0
   * @since 2016-03-17 16:14:00
   * @author Yesid Durán G. (Yeligoth) <yesiddg2014@gmail.com>
   */
  public static function chunk($array, $size, $preserveKeys = false) {
    return array_chunk($array, $size, $preserveKeys);
  }

  /**
   * Devuelve un array unidimensional con los valores de una columna especifica.
   *
   * @param array $array El array multidimensional del cual se va a extraer la información.
   * @param string $column La columna que se quiere extraer.
   * @param mixed $indexKey La columna por la cual se re-indexa cada elemento extraido.
   * lo que indica que, cada elemento se reordenara dentro de su fragmento, si se pasa en true, mantendra los indices reales.
   * @return boolean
   * @version 1.0
   * @since 2016-03-17 16:14:00
   * @author Yesid Durán G. (Yeligoth) <yesiddg2014@gmail.com>
   */
  public static function searchByColumn($array, $column, $indexKey = null) {
    return array_column($array, $column, $indexKey);
  }
}